package cz.zcu.kiv.dicepoker;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import cz.zcu.kiv.dicepoker.game.CombinationResolver;
import cz.zcu.kiv.dicepoker.game.CombinationType;
import cz.zcu.kiv.dicepoker.game.dice.Dice;
import cz.zcu.kiv.dicepoker.game.dice.DiceValue;
import cz.zcu.kiv.dicepoker.game.Player;

public class CombinationResolverTest {

    Player player;

    @Before
    public void setUp(){
        Dice[] dices = new Dice[5];

        dices[0] = new Dice(null);
        dices[1] = new Dice(null);
        dices[2] = new Dice(null);
        dices[3] = new Dice(null);
        dices[4] = new Dice(null);
        //player = new Player(null,dices, "test", 500);
    }

    void prepereDiceValues(int v1, int v2, int v3, int v4, int v5){
        player.getDices()[0].setValue(new DiceValue(v1, 0));
        player.getDices()[1].setValue(new DiceValue(v2, 0));
        player.getDices()[2].setValue(new DiceValue(v3, 0));
        player.getDices()[3].setValue(new DiceValue(v4, 0));
        player.getDices()[4].setValue(new DiceValue(v5, 0));

    }


//    @Test
//    public void isFiveOFAKind(){
//        prepereDiceValues(1, 1, 1, 1, 1);
//        CombinationType combinationType = CombinationResolver.getInstance().ResolveCombination(player);
//        assertEquals(CombinationType.FIVE_OF_A_KIND, combinationType);
//    }
//
//    @Test
//    public void isFourOFAKind(){
//        prepereDiceValues(1, 1, 1, 1, 6);
//        CombinationType combinationType = CombinationResolver.getInstance().ResolveCombination(player);
//        assertEquals(CombinationType.FOUR_OF_A_KIND, combinationType);
//    }
//
//    @Test
//    public void isStraight_1(){
//        prepereDiceValues(1, 2, 3, 4, 5);
//        CombinationType combinationType = CombinationResolver.getInstance().ResolveCombination(player);
//        assertEquals(CombinationType.STRAIGHT, combinationType);
//    }
//
//    @Test
//    public void isStraight_2(){
//        prepereDiceValues(2, 3, 4, 5, 6);
//        CombinationType combinationType = CombinationResolver.getInstance().ResolveCombination(player);
//        assertEquals(CombinationType.STRAIGHT, combinationType);
//    }
//
//    @Test
//    public void isFullHouse(){
//        prepereDiceValues(1, 1, 1, 2, 2);
//        CombinationType combinationType = CombinationResolver.getInstance().ResolveCombination(player);
//        assertEquals(CombinationType.FULL_HOUSE, combinationType);
//    }
//
//    @Test
//    public void isThreeOfAKind(){
//        prepereDiceValues(1, 1, 1, 2, 3);
//        CombinationType combinationType = CombinationResolver.getInstance().ResolveCombination(player);
//        assertEquals(CombinationType.THREE_OF_A_KIND, combinationType);
//    }
//
//    @Test
//    public void isTwoPair(){
//        prepereDiceValues(1, 1, 2, 2, 6);
//        CombinationType combinationType = CombinationResolver.getInstance().ResolveCombination(player);
//        assertEquals(CombinationType.TWO_PAIR, combinationType);
//    }
//
//    @Test
//    public void isOnePair(){
//        prepereDiceValues(3, 3, 5, 6, 2);
//        CombinationType combinationType = CombinationResolver.getInstance().ResolveCombination(player);
//        assertEquals(CombinationType.ONE_PAIR, combinationType);
//    }
}
