package cz.zcu.kiv.dicepoker;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void random_test(){
        Random rnd = new Random();
        int randomNum = 0;
        for(int i = 0; i < 100;i++){
             randomNum = (rnd.nextInt(6) + 1) ;
            System.out.println(randomNum);
        }
        assertEquals(5, randomNum);
    }
}