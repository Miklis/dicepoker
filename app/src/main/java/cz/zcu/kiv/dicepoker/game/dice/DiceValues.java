package cz.zcu.kiv.dicepoker.game.dice;

import java.util.InputMismatchException;

import cz.zcu.kiv.dicepoker.R;

public class DiceValues {
    /* Jediná instance třídy */
    private static DiceValues INSTANCE = null;

    /* Instance všech možných hodnot 6 hranné kostky */
    private final DiceValue[] values = new DiceValue[6];


    /**
     * Konstroktor vytvoří novou instanci hodnot
     * kostek, kdy vytvoří všech 6 možných hodnot
     * 6 hranné kostky
     */
    private DiceValues() {
        values[0] = new DiceValue(1 , R.drawable.ic_dice_one);
        values[1] = new DiceValue(2 , R.drawable.ic_dice_two);
        values[2] = new DiceValue(3 , R.drawable.ic_dice_three);
        values[3] = new DiceValue(4 , R.drawable.ic_dice_four);
        values[4] = new DiceValue(5 , R.drawable.ic_dice_five);
        values[5] = new DiceValue(6 , R.drawable.ic_dice_six);
    }

    /**
     * Vrátí jedinou instanci třídy
     *
     * @return instance třídy
     */
    public static DiceValues getInstance(){
        if(INSTANCE == null){
            INSTANCE = new DiceValues();
        }

        return INSTANCE;
    }

    /***
     * Je předpokládaný vstup 1-6 aby se vrýtila odpovídající hodnota kostky
     *
     * @param value
     * @return
     */
    public DiceValue getValue(int value){
        //pole začíná od nulu je pořeba o 1 zmenšit
        value--;
        if(value < 0 || value > 5){
            throw new InputMismatchException("Outbount value");
        }

        return values[value];
    }
}
