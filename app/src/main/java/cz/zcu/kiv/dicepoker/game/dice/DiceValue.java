package cz.zcu.kiv.dicepoker.game.dice;

import java.util.Objects;

public class DiceValue {
    /* Číselná hodnota kostky */
    public final int value;

    /* Klíč k odkazu na obrázek kostky s danou hodnotou */
    public final int resource;

    /**
     * Konstrukto vytvoří novou hodnotu kostky
     * s její hodnotou a id k obrázku s hodnotou
     *
     * @param value hodnota kostky
     * @param resource odkaz na obrázek
     */
    public DiceValue(int value, int resource) {
        this.value = value;
        this.resource = resource;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DiceValue diceValue = (DiceValue) o;
        return value == diceValue.value &&
                resource == diceValue.resource;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, resource);
    }
}
