package cz.zcu.kiv.dicepoker.game;

public enum SoundTypes {
    SHAKE_1,
    SHAKE_2,
    SHAKE_3,
    SHAKE_4,
    SHAKE_5,
    ROLL_1,
    ROLL_2,
    ROLL_3,
    ROLL_4,
    ROLL_5,
    BET_SMALL,
    BET_BIG,
}
