package cz.zcu.kiv.dicepoker.game.dice;

import android.graphics.Color;
import android.widget.ImageView;

import java.util.Objects;

/***
 * Třída představující hrací kostku a to její hodnotu a obrázek, kde je vyobrazena
 * aktuální hozená hodnota na kostce.
 */
public class Dice {
    /* Reference na obrázek kostky, kdy obrázek se může měnit ale
    * image resource. */
    public final ImageView diceResource;
    /* Reference na hodnotu kostky která obsahuje číselnou hodnotu a
    * číselný klíč k obrázku zobrazující stejnou hodnotu */
    private DiceValue value;

    /* Logická proměnná určující jestli je kostka v nějaké kombinaci
    * pár, dva páry, atd. */
    private boolean isInCombination;

    /* Logická proměnná určující jestli je kostka vybraná */
    private boolean isSelected;

    /**
     * Konstruktor vytvářející novou instanci kostky
     * kdy jí dá referenci na image view a počáteční hodnotu
     * kostky nastací na 1
     *
     * @param diceResource reference na image view kostky
     */
    public Dice(ImageView diceResource) {
        this.diceResource = diceResource;
        this.value = DiceValues.getInstance().getValue(1);
        this.diceResource.setImageResource(value.resource);
        this.isInCombination = false;
    }

    /**
     * Vrátí referenci na image view kostky.
     *
     * @return reference na image view kostky
     */
    public ImageView getDiceResource() {
        return diceResource;
    }

    /**
     * Nastaví hodkotu kostky což znamená její číselnou hodnotu
     * a tu samou hodnotu na obrázků
     *
     * @param value nová hodnota kostky
     */
     public void setValue(DiceValue value) {
        this.value = value;
        this.diceResource.setImageResource(value.resource);
    }

    /**
     * Vrátí hodnotu kostky
     *
     * @return hodnota kostky
     */
    public DiceValue getValue() {
        return this.value;
    }

    /**
     * Vrátí jestli je kostka v kombinaci
     *
     * @return je v kombinaci?
     */
    public boolean isInCombination() {
        return isInCombination;
    }

    /**
     * Nastaví jesli je kostka v kombinace
     *
     * @param inCombination je kostka v kombinaci?
     */
    public void setInCombination(boolean inCombination) {
        isInCombination = inCombination;
    }

    /**
     * Vrátí jestli je kostka označená
     *
     * @return je označená?
     */
    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    /**
     * Označí kostku, což znamená že jí prohlásí za označenou
     * a změní barvu překryvu obrázku
     */
    public void select(){
        isSelected = true;
        diceResource.setColorFilter(Color.argb(50, 100, 0, 0));
    }

    /**
     * Odznačí kostku. Řekne že není označená
     * a odstraní barvu překryvu.
     */
    public void unselect(){
        isSelected = false;
        diceResource.setColorFilter(Color.argb(0, 0, 0, 0));
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dice dice = (Dice) o;
        return isInCombination == dice.isInCombination &&
                isSelected == dice.isSelected &&
                Objects.equals(diceResource, dice.diceResource) &&
                Objects.equals(value, dice.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(diceResource, value, isInCombination, isSelected);
    }
}

