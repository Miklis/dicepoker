package cz.zcu.kiv.dicepoker.fragments;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import cz.zcu.kiv.dicepoker.R;
import cz.zcu.kiv.dicepoker.activities.PlayActivity;
import cz.zcu.kiv.dicepoker.game.CombinationType;
import cz.zcu.kiv.dicepoker.game.Game;
import cz.zcu.kiv.dicepoker.game.Player;


/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class PlayEndRoundFragment extends Fragment {
    /*
     *Instance hry
     */
    private static Game game = Game.getInstance();

    /**
     * Tlačítko pro konec hry
     */
    private  Button endGame;

    /**
     * Tlačítko pro další kolo
     */
    private Button nextRound;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_play_end_round, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.endGame = view.findViewById(R.id.button_end_game);
        this.nextRound = view.findViewById(R.id.button_next_round);
        setEndRoundGameInfo(game.getAiPlayer(), game.getPlayer(), view);
        createButtonsListeners();
    }

    /**
     * Nastaví informace o konci kola
     *
     * @param ai instance umělé inteligence
     * @param player instance hráče
     * @param view view
     */
    private void setEndRoundGameInfo(Player ai, Player player, View view){
        TextView player2WinComb = view.findViewById(R.id.textView_p2_win_combination);
        setWonImageView(ai.getRoundWins(), R.id.imageView_p2_matches_wins, view);
        String p2Comb = CombinationType.getString(ai.getCombinationType());
        int p2Score = ai.getScore();
        player2WinComb.setText(getString(R.string.comb_message, p2Comb, p2Score));

        TextView player1WinComb = view.findViewById(R.id.textView_p1_win_combination);
        String p1Comb = CombinationType.getString(player.getCombinationType());
        int p1Score = player.getScore();
        player1WinComb.setText(getString(R.string.comb_message, p1Comb, p1Score));
        setWonImageView(player.getRoundWins(), R.id.imageView_p1_matches_wins, view);

        TextView pool = view.findViewById(R.id.textView_pool_money);
        pool.setText(getString(R.string.pool, game.getMoneyInGame()));

        wonResume(ai, player, view);
    }

    /**
     * Vyhodnotí jestli se má upravit okno na výherní místo na info o konci kola
     *
     * @param ai umělá inteligence
     * @param player hráč
     * @param view view
     */
    private void wonResume(Player ai, Player player, View view){
        TextView wons = view.findViewById(R.id.textView_game_res);
        if(ai.getRoundWins() == 2 || player.getRoundWins() == 2){
            endGame.setVisibility(View.VISIBLE);
            wons.setVisibility(View.VISIBLE);
            nextRound.setVisibility(View.GONE);
            setGameState(true, view);
            if(ai.getRoundWins() == 2){
                wons.setText(getString(R.string.gameWinnerAI));
            }else {
                wons.setText(getString(R.string.gameWinnerPlayer));
            }
        }else {
            wons.setVisibility(View.INVISIBLE);
            endGame.setVisibility(View.GONE);
            nextRound.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Nastaví nadpis okna kdo vyhrál
     *
     * @param aiWin vyhrál ai? true ano false ne
     * @param view view
     */
    private void setGameState(boolean aiWin, View view){
        TextView gameState = view.findViewById(R.id.textView_game_res);
        if(aiWin){
            gameState.setText(getString(R.string.gameWinnerAI));
        }else {
            gameState.setText(getString(R.string.gameWinnerPlayer));
        }

    }

    /**
     * Nastaví obrázek s počtem vítězství hráče
     *
     * @param wonsNumber počet vítětství
     * @param wonsId id obrázku
     * @param view view
     */
    private void setWonImageView(int wonsNumber, int wonsId, View view){
        ImageView wons = view.findViewById(wonsId);

        if(wonsNumber == 0){
            wons.setImageResource(R.mipmap.no_win_foreground);
        }else if(wonsNumber == 1){
            wons.setImageResource(R.mipmap.one_win_foreground);
        }else if(wonsNumber == 2){
            wons.setImageResource(R.mipmap.two_win_foreground);
        }
    }

    /**
     * Vytvoří listenery na tlačítkách
     */
    private void createButtonsListeners(){
        nextRound();
        endGame();
    }

    /**
     * Nastaví listener na tlačítko pro další kolo
     */
    private void nextRound(){
        nextRound.setOnClickListener(v1 -> {
            Game.getInstance().prepareStartRound();
            Activity activity = null;
            if((activity = getActivity()) instanceof PlayActivity){
                ((PlayActivity)activity).closeFragment(PlayEndRoundFragment.class.getName());
                game.getAiPlayer().resetScore();
                game.getPlayer().resetScore();
            }else {
                Toast.makeText(getContext(), getString(R.string.error), Toast.LENGTH_LONG).show();

            }
        });
    }

    /**
     * Nastaví listener na tlačítko pro konec hry
     */
    private void endGame(){
        endGame.setOnClickListener(v1 -> {
            Activity activity = null;
            if((activity = getActivity()) instanceof PlayActivity){
                ((PlayActivity)activity).showBackDialog("Chceš se vártit do menu?", "Konec hry");
            }else {
                Toast.makeText(getContext(), getString(R.string.error), Toast.LENGTH_LONG).show();
            }
        });
    }
}
