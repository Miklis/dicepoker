package cz.zcu.kiv.dicepoker.activities;

import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;

import cz.zcu.kiv.dicepoker.R;
import cz.zcu.kiv.dicepoker.game.GameSoundPlayer;

public class MainMenuActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    protected void onStart() {
        super.onStart();
        GameSoundPlayer.setContext( getApplicationContext());
        GameSoundPlayer.getInstance().playMainMenuSong();
    }


    @Override
    protected void onPause() {
        super.onPause();
        GameSoundPlayer.getInstance().stopMainMenuSong();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Pokud uživatel nebude chtít tak se nezobrazí systémová tlačítka
     *
     * @param hasFocus
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus){
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }
}
