package cz.zcu.kiv.dicepoker.game;

public class Combination {
    /**
     * Typ hozené kombinace
     */
    public final CombinationType type;

    /**
     * Hodnota hozené kombinace
     */
    public final int combinationScore;

    /**
     * Vytvoří novou kombinaci
     *
     * @param type typ hozené kombinace
     * @param combinationScore hodnota kombinace
     */
    public Combination(CombinationType type, int combinationScore){
        this.type = type;
        this.combinationScore =combinationScore;
    }
}
