package cz.zcu.kiv.dicepoker.game;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;

import java.util.HashMap;
import java.util.Random;

import cz.zcu.kiv.dicepoker.R;

public class GameSoundPlayer {
    /* Proměnná určující jestli je zaplá muzika pozadí */
    public static boolean musicOn = true;

    /* Proměnná říkající jestli jsou zaplé zvuky */
    public static boolean soundsOn = true;

    public static HashMap<SoundTypes, Integer> soundsLength = new HashMap();

    /* Context aplikace */
    private static Context context;

    /* Jediná instance třídy */
    private static GameSoundPlayer instance;

    /* Instance pro generování náhodných čísel */
    private static Random rnd = new Random();

    /* Song1 pro hlavní menu */
    private MediaPlayer mainMenuSong1;

    /* Song2 pro halvní menu */
    private MediaPlayer mainMenuSong2;

    /* Song pro herní aktivitu */
    private MediaPlayer gameSong;

    /* Byl spouštěn song1 v menu */
    private boolean mainMenuSongIsStarted1 = false;

    /* Byl spouštěn song2 v menu */
    private boolean mainMenuSongIsStarted2 = false;

    /**
     * Vytvoří hash mapu s délkami songů
     */
    public void createSoundsLengthHashMap() {
        soundsLength.put(SoundTypes.BET_SMALL, songDuration(R.raw.bet_small));
        soundsLength.put(SoundTypes.BET_BIG, songDuration(R.raw.bet_big));

        soundsLength.put(SoundTypes.SHAKE_1, songDuration(R.raw.shake_one_dice));
        soundsLength.put(SoundTypes.SHAKE_2, songDuration(R.raw.shake_two_dice));
        soundsLength.put(SoundTypes.SHAKE_3, songDuration(R.raw.shake_three_dice));
        soundsLength.put(SoundTypes.SHAKE_4, songDuration(R.raw.shake_four_dice));
        soundsLength.put(SoundTypes.SHAKE_5, songDuration(R.raw.shake_five_dice));

        soundsLength.put(SoundTypes.ROLL_1, songDuration(R.raw.roll_one_dice));
        soundsLength.put(SoundTypes.ROLL_2, songDuration(R.raw.roll_two_dice));
        soundsLength.put(SoundTypes.ROLL_3, songDuration(R.raw.roll_three_dice));
        soundsLength.put(SoundTypes.ROLL_4, songDuration(R.raw.roll_four_dice));
        soundsLength.put(SoundTypes.ROLL_5, songDuration(R.raw.roll_five_dice));
    }



    /**
     * Vytvoří novou instanci soundPlayeru, jdy vytvoří
     * mediaPlayery pro songy v menu
     */
    private GameSoundPlayer(){
        if(context != null){
            mainMenuSong1 = MediaPlayer.create(context, R.raw.witcher_main_menu_song_one);
            mainMenuSong2 = MediaPlayer.create(context, R.raw.witcher_main_menu_song_two);
            createSoundsLengthHashMap();
        }

    }

    /**
     * Vráří jedinou instanci třídy
     *
     * @return jediná instance třídy
     */
    public static GameSoundPlayer getInstance(){
        if(instance == null){
            instance = new GameSoundPlayer();
        }
        return instance;
    }

    /**
     * Nastaví context třídě, který je pak nezbytně pro
     * prováděný některých operací v metodách
     *
     * @param context context
     */
    public static void setContext(Context context){
        GameSoundPlayer.context = context;
    }

    /**
     * Nastaví že má být zaplá muzika v pozadí
     *
     * @param on má se zapnout nebo vypnout
     */
    public static void setMusicOn(boolean on){
        musicOn = on;
        if(!on){
            getInstance().stopMainMenuSong();
            getInstance().stopGameSong();
        }else{
            GameSoundPlayer.getInstance().playMainMenuSong();
        }
    }

    /**
     * Nastaví že se mají pouštět zvuky aplikace
     *
     * @param on májí se spouštět nebo ne
     */
    public static void setSoundsOn(boolean on){
        soundsOn = on;
    }

    /**
     * Zapne náhodně jeden ze songů v main menu
     */
    public void playMainMenuSong(){
        int num = rnd.nextInt(2);

        if(musicOn && mainMenuSong1 != null && mainMenuSong2 != null){
            switch (num){
                case 0:
                    if(mainMenuSongIsStarted1 == true){
                        mainMenuSong1.start();
                    }else {
                        mainMenuSongIsStarted1 = true;
                        mainMenuSong1.setLooping(true);
                        mainMenuSong1.start();
                    }
                    break;
                case 1:
                    if(mainMenuSongIsStarted2 == true){
                        mainMenuSong2.start();
                    }else {
                        mainMenuSongIsStarted2 = true;
                        mainMenuSong2.setLooping(true);
                        mainMenuSong2.start();
                    }
                    break;
            }
        }
    }

    /**
     * Podle id songu vrátí jeho délku
     *
     * @param id id songu
     * @return délka songu
     */
    private int songDuration(int id){
        MediaPlayer mp =  MediaPlayer.create(context, id);
        int dur = mp.getDuration();
        mp.release();
        return dur;
    }

    /**
     * Vrátí délku znělky sázky
     *
     * @param bet
     * @return délka znělky
     */
    public int getBetsSoundDuration(Bet bet){
        switch (bet){
            case BIG:
                return soundsLength.get(SoundTypes.BET_BIG);
            case SMALL:
                return soundsLength.get(SoundTypes.BET_SMALL);
        }

        return 0;
    }

    /**
     * Vrátí délku znělky kvrdlání kostek
     *
     * @param number počet kostek
     * @return vrátí délku znělky
     */
    public int getShakeSoundDuration(int number){
        if(number > 0 && number <= 5){
            switch (number){
                case 1:
                    return soundsLength.get(SoundTypes.SHAKE_1);
                case 2:
                    return soundsLength.get(SoundTypes.SHAKE_2);
                case 3:
                    return soundsLength.get(SoundTypes.SHAKE_3);
                case 4:
                    return soundsLength.get(SoundTypes.SHAKE_4);
                case 5:
                    return soundsLength.get(SoundTypes.SHAKE_5);
            }
        }
        return 0;
    }

    /**
     * Vrátí délku znělky kutálení kostek
     *
     * @param number počet kostek
     * @return délka znělky
     */
    public int getRollSoundDuration(int number){
        if(number > 0 && number <= 5){
            switch (number){
                case 1:
                    return soundsLength.get(SoundTypes.ROLL_1);
                case 2:
                    return soundsLength.get(SoundTypes.ROLL_2);
                case 3:
                    return soundsLength.get(SoundTypes.ROLL_3);
                case 4:
                    return soundsLength.get(SoundTypes.ROLL_4);
                case 5:
                    return soundsLength.get(SoundTypes.ROLL_5);
            }
        }
        return 0;
    }


    /**
     * Zastaví songy v main menu
     */
    public void stopMainMenuSong(){
        if(mainMenuSong1 != null && mainMenuSong1.isPlaying()){
            mainMenuSong1.pause();
        }else if(mainMenuSong2 != null && mainMenuSong2.isPlaying()){
            mainMenuSong2.pause();
        }
    }

    /**
     * Zapne herní song
     */
    public void playGameSong(){
        if(musicOn){
            gameSong = MediaPlayer.create(context, R.raw.witcher_game_song);
            gameSong.start();
            gameSong.setLooping(true);
        }
    }

    /**
     * Zastaví herní song
     */
    public void stopGameSong(){
        if(gameSong != null && gameSong.isPlaying()){
            gameSong.stop();
            gameSong.release();
            gameSong = null;
        }
    }


    /**
     * Podle počtu kostek zapne zvuk kvrdlání kostek
     *
     * @param numberOfDice počet kostek
     */
    public void shakeDiceSound(int numberOfDice){
        Handler h = new Handler();
        h.post(() -> {
            if (soundsOn && context != null) {
                MediaPlayer shakeMP = null;
                switch (numberOfDice) {
                    case 1:
                        shakeMP = MediaPlayer.create(context, R.raw.shake_one_dice);
                        break;
                    case 2:
                        shakeMP = MediaPlayer.create(context, R.raw.shake_two_dice);
                        break;
                    case 3:
                        shakeMP = MediaPlayer.create(context, R.raw.shake_three_dice);
                        break;
                    case 4:
                        shakeMP = MediaPlayer.create(context, R.raw.shake_four_dice);
                        break;
                    case 5:
                        shakeMP = MediaPlayer.create(context, R.raw.shake_five_dice);
                        break;
                }

                if (shakeMP != null) {
                    shakeMP.start();
                    while (shakeMP.isPlaying()){
                        //wait
                    }
                    shakeMP.release();
                }

            }
        });
    }

    /**
     * Zapne zvuk dopadu kostek podle jejich počtu
     *
     * @param numberOfDice počet kostek
     */
    public void rollDiceSound(int numberOfDice){
        Handler h = new Handler();
        h.post(() -> {
            if (soundsOn && context != null) {
                MediaPlayer rollMP = null;
                switch (numberOfDice) {
                    case 1:
                        rollMP = MediaPlayer.create(context, R.raw.roll_one_dice);
                        break;
                    case 2:
                        rollMP = MediaPlayer.create(context, R.raw.roll_two_dice);
                        break;
                    case 3:
                        rollMP = MediaPlayer.create(context, R.raw.roll_three_dice);
                        break;
                    case 4:
                        rollMP = MediaPlayer.create(context, R.raw.roll_four_dice);
                        break;
                    case 5:
                        rollMP = MediaPlayer.create(context, R.raw.roll_five_dice);
                        break;
                }

                if (rollMP != null) {
                    rollMP.start();

                    while (rollMP.isPlaying()){
                        //wait
                    }
                    rollMP.release();
                }
            }
        });
    }

    /**
     * Spustí zvuky sázení peněz
     *
     * @param bet výše sázky
     */
    public void betSound(Bet bet){
        Handler h = new Handler();
        h.post(() -> {
            if (soundsOn && context != null) {
                MediaPlayer betMP = null;
                switch (bet) {
                    case NO:
                        break;
                    case SMALL:
                        betMP = MediaPlayer.create(context, R.raw.bet_small);
                        break;
                    case BIG:
                        betMP = MediaPlayer.create(context, R.raw.bet_big);
                        break;
                }

                if (betMP != null) {
                    betMP.start();

                    while (betMP.isPlaying()){
                        //wait
                    }
                    betMP.release();
                }
            }
        });
    }

}
