package cz.zcu.kiv.dicepoker.game;

public enum GamePhase {
    START_ROUND,
    BETS,
    REISE,
    ROLL_ALL,
    ROLL,
    END_ROUND,
    END_GAME;
}
