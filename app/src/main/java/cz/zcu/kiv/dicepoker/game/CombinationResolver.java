package cz.zcu.kiv.dicepoker.game;

import java.util.Arrays;

import cz.zcu.kiv.dicepoker.game.dice.Dice;

/**
 * Tato třída se stará o určení jakou kombinaci hráč hodil
 */
public class CombinationResolver {

    /* Jediná instance třídy */
    private static CombinationResolver INSTANC = null;

    /* Maximální hodnota kostky + 1 protože 0 index je
    * prázdný pro pole uchování kostek*/
    private static int DICE_MAX_VALUE = 6 + 1;

    //lenght 7 becouse we have range 1-6 and dont use 0
    private int[] combination;

    /* Pole kostek se kterými se házelo */
    private Dice[] dices;

    /**
     * Kombinace;
     */
    private Combination combi;

    /* Private konstruktor */
    private CombinationResolver(){}

    /**
     * Vrátí jedinou instanci trídy
     *
     * @return instance třídy
     */
    public static CombinationResolver getInstance(){
        if(INSTANC == null){
            INSTANC = new CombinationResolver() ;
        }
        return INSTANC;
    }

    /***
     * Metoda určí jakou kombinaci hráč hodil a hráči nastaví skóre
     * výsledné kombinace kostek.
     *
     * @param player hráč který házel a s jeho kostkami se bude zacházet
     */
    public void ResolveCombination(Player player){
        this.dices = player.getDices();
        CombinationType combinationType;
        combination = new int[DICE_MAX_VALUE];
        for(int i = 0; i < dices.length; i++){
            combination[dices[i].getValue().value]++;
        }
        player.resetInCombinationDices();

        if(isFiveOfAKind()){
            combinationType = CombinationType.FIVE_OF_A_KIND;
        }else if(isFourOfAKind()){
            combinationType = CombinationType.FOUR_OF_A_KIND;
        }else if(isFullHouse()){
            combinationType = CombinationType.FULL_HOUSE;
        }else if(isStraight()){
            combinationType = CombinationType.STRAIGHT;
        }else if(isThreeOfAKind()){
            combinationType = CombinationType.THREE_OF_A_KIND;
        }else if(isTwoPair()){
            combinationType = CombinationType.TWO_PAIR;
        }else if(isOnePair()){
            combinationType = CombinationType.ONE_PAIR;
        }else if(isHighestCard()){
            combinationType = CombinationType.HIGHEST_CARD;
        }else {
            combinationType = CombinationType.ERROR;
        }

        player.setScore(combi);
    }


    /**
     * Kontroluje jestli je pět stejných kostek a pokud ano
     * příslušné kostky jsou nastaveny jako že jsou v kombinaci
     *
     * @return true ano false ne
     */
    private boolean isFiveOfAKind(){
        for(int i = 1; i < combination.length; i++){
            if(combination[i] == 5){
                combi = new Combination (CombinationType.FIVE_OF_A_KIND, (i * 100) + 7000 );
                setInCombinationDicesWithValue(i);
                return true;
            }
        }
        return false;
    }

    /**
     * Kontroluje jestli jsou 4 stejné a pokud ano
     * příslušné kostky jsou nastaveny jako že jsou v kombinaci
     *
     * @return true ano false ne
     */
    private boolean isFourOfAKind(){
        int score = 0;
        for(int i = 1; i < combination.length; i++){
            if(combination[i] == 4){
                score = (i * 100) + 6000 ;
                combi = new Combination (CombinationType.FOUR_OF_A_KIND, score );
                setInCombinationDicesWithValue(i);
                return true;
            }
        }

        return false;
    }

    /**
     * Kontroluje jestli je to postupka a pokud ano
     * příslušné kostky jsou nastaveny jako že jsou v kombinaci
     *
     * @return true ano je to pustupka false ne
     */
    private boolean isStraight(){
        int check = 0;

        for(int i = 1; i < combination.length - 1; i++){
            if (combination[i] == 1 && combination[i+1] == 1) {
                check++;
            }else {
                check = 0;
            }

            if(check == 4){
                combi = new Combination (CombinationType.STRAIGHT, 4000);
                setInCombinationAllDices();
                return true;
            }
        }


        return false;
    }

    /**
     * Jedná se o full house, tedy dvě a tři stejné a pokud ano
     * příslušné kostky jsou nastaveny jako že jsou v kombinaci
     *
     * @return true ano false ne
     */
    private boolean isFullHouse(){
        int three = 0;
        int two = 0;
        int score = 0;

        for(int i = 1; i < combination.length;i++){
            if(combination[i] == 3){
                three = i;
                continue;
            }
            if(combination[i] == 2){
                two = i;
            }
        }

        if(three > 0 && two > 0){
            score = (three * 100) + (two * 100) + 5000 ;
            combi = new Combination (CombinationType.STRAIGHT, score);
            setInCombinationDicesWithValue(three);
            setInCombinationDicesWithValue(two);
            return true;
        }

        return false;
    }

    /***
     * Jedná se o tři stejné a pokud ano
     * příslušné kostky jsou nastaveny jako že jsou v kombinaci
     *
     * @return true ano false ne
     */
    private boolean isThreeOfAKind(){
        int three = 0;
        int two = 0;
        int score = 0;

        for(int i = 1; i < combination.length;i++){
            if(combination[i] == 3){
                three = i;
                continue;
            }
            if(combination[i] == 2){
                two = i;
            }
        }

        if(three > 0 && two == 0){
            score = (three * 100) + 3000;
            combi = new Combination (CombinationType.THREE_OF_A_KIND, score);
            setInCombinationDicesWithValue(three);
            return true;
        }

        return false;
    }

    /**
     * Jedná se o dva páry a pokud ano
     * příslušné kostky jsou nastaveny jako že jsou v kombinaci
     *
     * @return true ano false ne
     */
    private  boolean isTwoPair(){
        int first = 0;
        int second = 0;
        int score;

        for(int i = 1; i < combination.length; i++){
            if(first > 0 && combination[i] == 2){
                second = i;
                continue;
            }

            if(combination[i] == 2){
                first = i;
            }
        }

        if(first > 0 && second > 0){
            score = (first * 100) + (second * 100) + 2000;
            combi = new Combination (CombinationType.TWO_PAIR, score);
            setInCombinationDicesWithValue(first);
            setInCombinationDicesWithValue(second);
            return true;
        }else {
            return false;
        }
    }

    /***
     * Jedná se o jeden pár, tedy dvě kostky se stejnou hodnotou a pokud ano
     * příslušné kostky jsou nastaveny jako že jsou v kombinaci
     *
     * @return true ano false ne
     */
    private boolean isOnePair(){
        int score = 0;
        for(int i = 1; i < combination.length; i++){
            if(combination[i] == 2){
                score = (i * 100) + 1000;
                combi = new Combination (CombinationType.ONE_PAIR, score);
                setInCombinationDicesWithValue(i);
                return true;
            }
        }
        return false;
    }

    /**
     * Nepadlo niz jiného bere se pouze nejvyšší kostka a pokud ano
     * příslušné kostky jsou nastaveny jako že jsou v kombinaci
     *
     * @return true ano je to nejvyšší kostka false není
     */
    private boolean isHighestCard(){
        int score = 0;
        for(int i = combination.length - 1; i > 0; i--){
            if(combination[i] > 0){
                score = i * 100;
                combi = new Combination (CombinationType.HIGHEST_CARD, score);
                setInCombinationDicesWithValue(i);
                break;
            }
        }
        return true;
    }

    /***
     * Nastaví kostku s konrétní hodnotou na to že je v kombinaci
     *
     * @param value hodnota kostky
     */
    private void setInCombinationDicesWithValue(int value){
        for(Dice dice : dices){
            if(dice.getValue().value == value){
                dice.setInCombination(true);
            }
        }
    }

    /***
     * Nastaví všechny kostky na to že jsou v kombinaci
     */
    private void setInCombinationAllDices(){
        for(Dice dice : dices){
            dice.setInCombination(true);
        }
    }


}
