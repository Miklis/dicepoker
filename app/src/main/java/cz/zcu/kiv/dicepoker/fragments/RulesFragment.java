package cz.zcu.kiv.dicepoker.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import cz.zcu.kiv.dicepoker.R;


/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class RulesFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rules, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        backButton(view);
    }

    /**
     * Nataví listener na tlačítko zpět
     * @param view view
     */
    private void backButton(View view){
        Button backBtn = view.findViewById(R.id.button_back_rules);
        backBtn.setOnClickListener(v -> NavHostFragment
                .findNavController(RulesFragment.this)
                .navigate(R.id.action_rules_fragment_to_main_menu_fragment));
    }
}
