package cz.zcu.kiv.dicepoker;

import androidx.fragment.app.Fragment;

import java.util.HashMap;

/**
 * Třída pro správu fragmetů
 */
public class FragmentBox {
    /**
     * Jedinná instance třídy
     */
    private static FragmentBox Instance = null;

    private FragmentBox(){}

    /**
     * Vrátí instanci jedináčka
     *
     * @return instance třídy
     */
    public static FragmentBox getInstance(){
        if(Instance == null){
            Instance = new FragmentBox();
        }
        return Instance;
    }

    /* Hashmapa na odkazy na instance fragmentů*/
    private HashMap<String, Fragment> fragments = new HashMap<>();

    /***
     * Přidá fragment do hashmapy
     *
     * @param name jméno fragmentu
     * @param fragment fragment
     */
    public void addFragment(String name, Fragment fragment){
        fragments.put(name, fragment);
    }

    /**
     * Získá fragment z hashmapy
     *
     * @param name název fragmentu
     * @return vrátí fragment
     */
    public Fragment getFragment(String name){
        return fragments.get(name);
    }
}
