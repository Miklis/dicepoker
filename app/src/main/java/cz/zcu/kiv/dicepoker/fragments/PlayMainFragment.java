package cz.zcu.kiv.dicepoker.fragments;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import cz.zcu.kiv.dicepoker.FragmentBox;
import cz.zcu.kiv.dicepoker.R;
import cz.zcu.kiv.dicepoker.activities.PlayActivity;
import cz.zcu.kiv.dicepoker.game.Game;
import cz.zcu.kiv.dicepoker.game.dice.Dice;

public class PlayMainFragment extends Fragment {

    private static Game game = Game.getInstance();

    private ConstraintLayout rollingInfo;

    private LinearLayout diceInMidleLayout;

    private ImageView[] diceInMiddle = new ImageView[5];

    private Button rollButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentBox.getInstance().addFragment(PlayMainFragment.class.getName(), this);
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_play_main, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FragmentBox.getInstance().addFragment(this.getClass().getName(), this);
        createButtonsListeners(view);
        addResourceToImageViewDicesInMiddle(view);
        addResourceToLayoutReferences(view);
        game.setPlayMainFragment(this);
    }

    /**
     * Pridá onTouch listener na kostky
     *
     * @param dices kostky
     */
    @SuppressLint("ClickableViewAccessibility")
    public void addListenersToDiceImageViews(Dice[] dices){
        for (Dice dice : dices) {
            ImageView image = dice.diceResource;
            image.setOnTouchListener((v, event) -> {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (dice.isSelected()) {
                        dice.unselect();
                    } else {
                        dice.select();
                    }
                    new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                }
                return false;
            });
        }
    }

    /**
     * Odstraní listenery na obrázkách kostek
     *
     * @param dices kostky
     */
    @SuppressLint("ClickableViewAccessibility")
    public void removeListenerFromDiceImageViews(Dice[] dices){
        for(Dice dice : dices){
            ImageView image = dice.diceResource;
            image.setColorFilter(Color.argb(0, 0, 0, 0));
            image.setOnTouchListener(null);
        }
    }

    /**
     * Vypne nebo zapne házecí tlačítko
     *
     * @param enable true zapnout false vypnout
     */
    public void setRollButtonInteractive(boolean enable){
        rollButton.setEnabled(enable);
    }

    /**
     * Zobrazí layout s gifem že hází ai
     *
     * @param show zobraz true skryj false
     */
    public void showRollingGifInfo(boolean show){
        if(show){
            rollingInfo.setVisibility(View.VISIBLE);
        }else {
            rollingInfo.setVisibility(View.GONE);
        }
    }

    /**
     * Nastaví množšství peněz u hráče
     *
     * @param moneyAmount počet peněz
     */
    public void setPlayerMoneyAmount(int moneyAmount){
        TextView tv = getView().findViewById(R.id.textView_player_coin);
        tv.setText(getString(R.string.players_coint, moneyAmount));
    }

    /**
     * Nastaví množství peněz u umělé inteligence
     *
     * @param moneyAmount počet pěněz
     */
    public void setAiMoneyAmount(int moneyAmount){
        TextView tv = getView().findViewById(R.id.textView_ai_coin);
        tv.setText(getString(R.string.players_coint, moneyAmount));
    }

    /**
     * Získá odkazy na kostky ve středu fragmentu a uloží je do pole
     *
     * @param view view
     */
    private void addResourceToImageViewDicesInMiddle(View view){
        diceInMiddle[0] = view.findViewById(R.id.imageView_dice_game1);
        diceInMiddle[1] = view.findViewById(R.id.imageView_dice_game2);
        diceInMiddle[2] = view.findViewById(R.id.imageView_dice_game3);
        diceInMiddle[3] = view.findViewById(R.id.imageView_dice_game4);
        diceInMiddle[4] = view.findViewById(R.id.imageView_dice_game5);
    }

    /**
     * Získá odkazy na layout ve fragmentu
     *
     * @param view view
     */
    private void addResourceToLayoutReferences(View view){
        rollingInfo = view.findViewById(R.id.constraintLayout_rolling_info);
        diceInMidleLayout = view.findViewById(R.id.linearLayout_dice_in_middle);
    }

    /**
     * Vytvoří listenery na tlačítkách ve fragmentu
     *
     * @param view view
     */
    private void createButtonsListeners(View view){
       rollButton = view.findViewById(R.id.button_roll);
       randomizeDice(view);
    }

    /**
     * Hod kostkama
     *
     * @param v view
     */
    private void randomizeDice(View v){
        Button b = v.findViewById(R.id.button_roll);
        b.setOnClickListener(v1 -> {
            Handler h = new Handler();
            h.post(() -> {
                setRollButtonInteractive(false);
                game.refreshGame();
            });
        });
    }

}
