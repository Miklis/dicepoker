package cz.zcu.kiv.dicepoker.game;

import android.os.Handler;
import android.util.Log;

import java.util.Random;

import cz.zcu.kiv.dicepoker.fragments.PlayBetAiFragment;
import cz.zcu.kiv.dicepoker.fragments.PlayEndRoundFragment;
import cz.zcu.kiv.dicepoker.fragments.PlayMainFragment;
import cz.zcu.kiv.dicepoker.activities.PlayActivity;
import cz.zcu.kiv.dicepoker.fragments.PlayBetFragment;
import cz.zcu.kiv.dicepoker.game.ai.EasyAIPlayer;

public class Game {
    /**
     * Výše nulové sázky
     */
    public static final int NO_BLIND = 0;

    /**
     * Výše malé sázky
     */
    public static final int SMALL_BLIND = 40;

    /**
     * Výše velké sázky
     */
    public static final int BIG_BLIND = 80;

    /**
     * Délka v ms jak má dlouho čekat AI
     */
    private static final int AI_WAIT = 2500;

    /**
     * Čas v ms pro zbržďění vláken
     */
    private static final int SHORT_WAIT = 1000;

    /**
     * Čas v ms pro zbrždění vláken
     */
    private static final int LONG_WAIT = 2000;

    /**
     * Jediná instance
     */
    private static Game INSTANCE = null;

    /**
     * Instance přehrávače hudby
     */
    private GameSoundPlayer gameSoundPlayer;

    /* Pomocná proměnná pro odkaz na instanci hráče který začal*/
    private Player firstPlayer;

    /* Umělá inteligence protivníka */
    private EasyAIPlayer aiPlayer;

    /* Reference na hráče */
    private Player player;

    /**
     * Hlavní herní aktivita
     */
    private PlayActivity playActivity;

    /**
     *Instance hlavního frakmentu herní aktivity
     */
    private PlayMainFragment playMainFragment;

    /* Reference na hráče který je aktuálně na řadě */
    private Player currentPlayer;

    //Hra má několik fází od počáteční, kdy jeden hází a druhý čeká
    private GamePhase gamePhase;

    /**
     * Vsazené peníze
     */
    private int moneyInGame;

    /**
     * Poslední sázka od hráče nebo počítače
     */
    private int currentBet;

    /**
     * Proběhlo již první kolo?
     */
    private boolean startRoundPassed;

    /**
     * Vytvoří novou instanci hry
     */
    private Game(){
        this.gameSoundPlayer = GameSoundPlayer.getInstance();
    }

    /**
     * Vratí jedinou instanci třídy
     * @return
     */
    public static Game getInstance(){
        if(INSTANCE == null){
            INSTANCE = new Game();
        }

        return INSTANCE;
    }

    /**
     * Vytvoří novou hru
     *
     * @param ai instance ai
     * @param player instance hráče
     * @param playActivity instance herní aktivity
     */
    public void setNewGame(EasyAIPlayer ai, Player player, PlayActivity playActivity){
        this.aiPlayer = ai;
        this.player = player;
        this.playActivity = playActivity;
        this.firstPlayer = player;
        this.currentPlayer = firstPlayer;
        this.gamePhase = GamePhase.START_ROUND;
        this.moneyInGame = 0;
    }

    /**
     * Vrátí instnaci umělé inteligence
     *
     * @return ai
     */
    public Player getAiPlayer(){
        return aiPlayer;
    }

    /**
     * Vrátí instanci hráče
     *
     * @return hráč
     */
    public Player getPlayer(){
        return player;
    }

    /**
     * Vrátí počet vsazených peněz
     *
     * @return počet vsazených peněz
     */
    public int getMoneyInGame(){
        return moneyInGame;
    }

    /**
     * Vrátí fázi hry ve které se nachází
     *
     * @return
     */
    public GamePhase getGamePhase(){
        return gamePhase;
    }


    /**
     * Nastaví instanci hlavního herní fragmentu
     *
     * @param playMainFragment hlavní herní fragment
     */
    public void setPlayMainFragment(PlayMainFragment playMainFragment) {
        this.playMainFragment = playMainFragment;
    }


    /**
     * Nejdůležitější metoda třídy starající se o změnu stavu hry.
     * Probíhá zde vyhodnocování jaká akce se má provést a spouští
     * nezbytné metody pro hru.
     */
    public void refreshGame() {
        debugLogs();
        int wait;
            if(gamePhase == GamePhase.START_ROUND ){
                basicBet();
                aiBet(currentBet, true);
                startRoundPassed = true;
            } else if(gamePhase == GamePhase.ROLL_ALL){
                wait =rollAll(player);;
                Handler handler = new Handler();
                handler.postDelayed(this::aiRoll,wait);
            } else if(gamePhase == GamePhase.BETS){
                basicBet();
                aiBet(currentBet, false);
                startRoundPassed = false;
            } else if(gamePhase == GamePhase.REISE){
                basicBet();
                fromReiseToRolling(startRoundPassed);
            } else if(gamePhase == GamePhase.ROLL){
                wait = rollSelected(player);
                Handler handler = new Handler();
                handler.postDelayed(this::aiRoll, wait);
            }
    }

    /**
     * Zprosdředkovává sázené
     *
     * @param bet výše sázky
     * @param player hráč který sází
     */
    public void bet(Bet bet, Player player){
        switch (bet){
            case NO:
                currentBet = player.bet(NO_BLIND);
                break;
            case SMALL:
                currentBet = player.bet(SMALL_BLIND);
                break;
            case BIG:
                currentBet = player.bet(BIG_BLIND);
                break;
        }

    }

    /**
     * Připraví hru na nové kole
     */
    public void prepareStartRound(){
        currentPlayer = firstPlayer;
        playMainFragment.setRollButtonInteractive(false);
        preparePlayerForStartRound(aiPlayer);
        preparePlayerForStartRound(player);
        gamePhase = GamePhase.START_ROUND;
        playActivity.openFragment(PlayBetFragment.class.getName());
    }

    /**
     * poslední sázku připočte k penězům v poolu
     */
    public void basicBet(){
        moneyInGame += currentBet;
    }

    /**
     * Připravý hráče na nové kolo
     *
     * @param player hráč
     */
    private void preparePlayerForStartRound(Player player){
        player.resetScore();
        player.unHighlightDices();
        player.resetDicesValue();
        player.resetInCombinationDices();
    }


    /**
     * Předaný hráč hodí vybranýma kostkama
     *
     * @param player instance hráče
     *
     * @return čas hodu pro zbrždění funkcí
     */
    private int rollSelected(Player player){
        int numberOfDices =  player.getNumberOfSelectedDices();
        gameSoundPlayer.shakeDiceSound(numberOfDices);
        int time1 = gameSoundPlayer.getShakeSoundDuration(numberOfDices);
        int time2 = gameSoundPlayer.getRollSoundDuration(numberOfDices);
        Handler h = new Handler();
        h.postDelayed(() -> {
            playMainFragment.removeListenerFromDiceImageViews(player.getDices());
            rolling(player);
            gameSoundPlayer.rollDiceSound(numberOfDices);
        },time1);

        return time1 + time2;
    }

    /**
     * Hod všemi kostkami
     *
     * @param player instance hráče
     *
     * @return čas hodu pro zbrždění funkcí
     */
    private int rollAll(Player player){
        player.selectAllDices();
        int time1 = gameSoundPlayer.getShakeSoundDuration(5);
        int time2 = gameSoundPlayer.getRollSoundDuration(5);
        gameSoundPlayer.shakeDiceSound(5);

        Handler h =  new Handler();
        h.postDelayed(() -> {
            rolling(player);
            gameSoundPlayer.rollDiceSound(5);
        }, time1);

        return time1 + time2;
    }

    /**
     * samotné házení kostkami a spuštění pod metod pro určení kombinace atd.
     *
     * @param player instance hráče
     */
    private void rolling(Player player){
        player.rollDices();
        player.unselectAllDices();
        CombinationResolver.getInstance().ResolveCombination(player);
        player.highlightBestCombination();
    }


    /**
     * Zobrazí info na konci kola
     */
    private void showEndRoundInfo(){
        playMainFragment.removeListenerFromDiceImageViews(aiPlayer.getDices());
        playMainFragment.removeListenerFromDiceImageViews(player.getDices());
        aiPlayer.highlightBestCombination();
        player.highlightBestCombination();
        playActivity.openFragment(PlayEndRoundFragment.class.getName());
    }

    /**
     * Provede akce nutné pro přechod mezi fází přihazování a
     * házením. Uzavírá a otvírá příslušné fregmenty
     *
     * @param fromStartPhase
     */
    private void fromReiseToRolling(boolean fromStartPhase){
        if(fromStartPhase == false){
            playMainFragment.setRollButtonInteractive(true);
            playMainFragment.addListenersToDiceImageViews(player.getDices());
            aiPlayer.unHighlightDices();
            player.unHighlightDices();
            gamePhase = GamePhase.ROLL;
        }else {
            playMainFragment.setRollButtonInteractive(true);
            gamePhase = GamePhase.ROLL_ALL;
            startRoundPassed = false;
        }
        playActivity.closeFragment(PlayBetFragment.class.getName());
    }

    /**
     * Připravý hru na házení všemi kostkami pokud ai nepřehodí hráče
     *
     * @return true připrav false ai přihodila nepřipravuj
     */
    private boolean prepareRollAll() {
        if (!isRaise()) {
            playMainFragment.setRollButtonInteractive(true);
            gamePhase = GamePhase.ROLL_ALL;
            return true;
        }else {
            return false;
        }
    }

    /**
     * Připravý hru na sázení
     */
    private void prepareBet(){
        Handler h1 = new Handler();
        h1.postDelayed(() -> {
            playMainFragment.setRollButtonInteractive(false);
            playActivity.openFragment(PlayBetFragment.class.getName());
            currentPlayer = firstPlayer;
            gamePhase = GamePhase.BETS;
        }, SHORT_WAIT);
    }

    /**
     * Připravý hru na házení vybranýma kostkama pokud ai nepřehodila hráče
     *
     * @return true připrav false ai přihodila nepřipravuj
     */
    private boolean prepareRollSelected() {
        if (!isRaise()) {
            playMainFragment.setRollButtonInteractive(true);
            playMainFragment.addListenersToDiceImageViews(player.getDices());
            aiPlayer.unHighlightDices();
            player.unHighlightDices();
            gamePhase = GamePhase.ROLL;
            return true;
        }else {
            return false;
        }
    }

    /**
     * Bylo přihoze?
     *
     * @return true bylo přihozeno false nebylo
     */
    private boolean isRaise(){
        if(aiPlayer.getCurrentBet() != player.getCurrentBet()) {
            gamePhase = GamePhase.REISE;
            return true;
        }

        return false;
    }

    /**
     * Připravý konec kola
     */
    private void prepareEndOfRound() {
        Handler h = new Handler();
        h.postDelayed(() -> {
            resumeWhoWinRound();
            showEndRoundInfo();
        }, LONG_WAIT);
    }

    /**
     * Vyhodnotí kdo z hráčů vyhrál
     */
    private void resumeWhoWinRound(){
        if(aiPlayer.getScore() > player.getScore()){
            aiPlayer.victoryInRound();
        }else {
            player.victoryInRound();
        }
    }


    /******************************* AI SECTION ******************************************/

    /***
     * Zprostředkuje akci sázení umělé inteligence.
     *
     * @param pool množství vsazených peněz v kole
     * @param startRound jedná se o první kolo sázek?
     */
    private void aiBet(int pool, boolean startRound){
            playActivity.closeFragment(PlayBetFragment.class.getName());
            playActivity.openFragment(PlayBetAiFragment.class.getName());

            Handler h = new Handler();
            h.postDelayed(() -> {
                int time =  aiPlayer.betAi(startRound, pool);
                Handler handler = new Handler();
                handler.postDelayed(() -> {
                    boolean prepareRol;
                    if(startRound){
                        prepareRol = prepareRollAll();
                    }else {
                        prepareRol = prepareRollSelected();
                    }
                    if (!prepareRol) {
                        playActivity.closeFragment(PlayBetAiFragment.class.getName());
                        playActivity.openFragment(PlayBetFragment.class.getName());
                    } else {
                        playActivity.closeFragment(PlayBetAiFragment.class.getName());
                    }
                }, time);

            }, AI_WAIT);
    }

    /**
     * Zprostředkovává házení kostkama umělé inteligence
     */
    private void aiRoll(){
        playMainFragment.showRollingGifInfo(true);
        Handler h = new Handler();
        h.postDelayed(() ->{
            int wait;
            if(gamePhase == GamePhase.ROLL_ALL){
                wait = rollAll(aiPlayer);
                Handler handler = new Handler();
                handler.postDelayed(this::prepareBet,wait);
            }else {
                Handler handler = new Handler();
                aiPlayer.selectedDices();
                wait = rollSelected(aiPlayer);
                handler.postDelayed(() -> {
                    prepareEndOfRound();
                },wait);
            }
            playMainFragment.showRollingGifInfo(false);
        }, AI_WAIT);
    }


    private void debugLogs(){
        Log.d("Player1: ", aiPlayer.toString());
        Log.d("Player2: ", player.toString());
        Log.d("GamePhase: ", gamePhase + "");
    }
}
