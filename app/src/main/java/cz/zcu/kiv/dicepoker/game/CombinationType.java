package cz.zcu.kiv.dicepoker.game;

import android.content.res.Resources;
import android.widget.Switch;

import cz.zcu.kiv.dicepoker.R;

public enum CombinationType {
    ERROR,
    HIGHEST_CARD,
    ONE_PAIR,
    TWO_PAIR,
    THREE_OF_A_KIND,
    FULL_HOUSE,
    STRAIGHT,
    FOUR_OF_A_KIND,
    FIVE_OF_A_KIND;

    public static String getString(CombinationType combinationType){
        switch (combinationType){
            case HIGHEST_CARD:
                return "Nejvyšší kostka";
            case ONE_PAIR:
                return "Pár";
            case TWO_PAIR:
                return "Dva Páry";
            case THREE_OF_A_KIND:
                return "Tři stejné";
            case STRAIGHT:
                return "Postupka";
            case FULL_HOUSE:
                return "Full house";
            case FOUR_OF_A_KIND:
                return "Čtyči stejné";
            case FIVE_OF_A_KIND:
                return "Pět stejných";
            default:
                return "Chyba";
        }
    }
}
