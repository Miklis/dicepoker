package cz.zcu.kiv.dicepoker.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;

import cz.zcu.kiv.dicepoker.R;
import cz.zcu.kiv.dicepoker.game.GameSoundPlayer;


public class SettingsFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return  inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListenersToGuiElements(view);
    }

    /**
     * Zavolá funkce pro nastavení listenerů na tlačítka a switche
     *
     * @param view view
     */
    private void setListenersToGuiElements(View view){
        soundsSwitch(view);
        musicSwitch(view);
        settingsBackButton(view);
    }

    /***
     * Nastaví listener na sound switch
     *
     * @param view view
     */
    private void soundsSwitch(View view) {
        Switch soundsSwitch = view.findViewById(R.id.switch_sounds_efects);
        soundsSwitch.setChecked(GameSoundPlayer.soundsOn);
        soundsSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            GameSoundPlayer.setSoundsOn(isChecked);
        });
    }

    /**
     * Nastaví tlačítko na switch pro muziku pozadí
     *
     * @param view vie
     */
    private void musicSwitch(View view){
        Switch musicSwitch = view.findViewById(R.id.switch_background_music);
        musicSwitch.setChecked(GameSoundPlayer.musicOn);
        musicSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            GameSoundPlayer.setMusicOn(isChecked);
        });
    }

    /***
     * Nastaví listener na tlačítko back
     *
     * @param view back
     */
    private void settingsBackButton(View view){
        Button settingsBtn = view.findViewById(R.id.button_back_settings);
        settingsBtn.setOnClickListener(v -> {
            NavHostFragment.findNavController(SettingsFragment.this)
                    .navigate(R.id.action_settings_fragment_to_main_menu_fragment);
        });
    }
}
