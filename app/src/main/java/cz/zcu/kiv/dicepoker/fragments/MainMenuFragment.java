package cz.zcu.kiv.dicepoker.fragments;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import cz.zcu.kiv.dicepoker.activities.PlayActivity;
import cz.zcu.kiv.dicepoker.R;
import cz.zcu.kiv.dicepoker.game.GameSoundPlayer;

public class MainMenuFragment extends Fragment {

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       setOnClickEventToAllButtons(view);
    }

    /**
     * Nastaví akce tlačítkům v menu
     *
     * @param view view
     */
    private void setOnClickEventToAllButtons(View view){
       playButton(view);
       rulesButton(view);
       settingsButton(view);
       quitButton(view);
    }

    /**
     * Nastaví akci co se má stát pokud se klikne na tlačítko Hrát
     *
     * @param view view
     */
    private void playButton(View view){
        Button playBtn = view.findViewById(R.id.button_play);
        playBtn.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), PlayActivity.class);
            startActivity(intent);
        });
    }

    /**
     * Nastaví akci co se má stát pokud se klikne na tlačítko  Jak hrát hru
     *
     * @param view view
     */
    private void rulesButton(View view){
        Button settingsBtn = view.findViewById(R.id.button_rules);
        settingsBtn.setOnClickListener(v ->
                NavHostFragment.findNavController(MainMenuFragment.this)
                    .navigate(R.id.action_main_menu_fragment_to_rules_fragment));
    }

    /**
     * Nastaví akci co se má stát pokud se klikne na tlačítko Nastavení
     *
     * @param view view
     */
    private void settingsButton(View view){
        Button settingsBtn = view.findViewById(R.id.button_settings);
        settingsBtn.setOnClickListener(v -> {
            NavHostFragment.findNavController(MainMenuFragment.this)
                    .navigate(R.id.action_main_menu_fragment_to_settings_fragment);
        });
    }

    /**
     * Nastaví akci co se má stát pokud se klikne na tlačítko konec
     *
     * @param view view
     */
    private void quitButton(View view){
        Button quitBtn = view.findViewById(R.id.button_quit);
        quitBtn.setOnClickListener(v -> {
            GameSoundPlayer.getInstance().stopMainMenuSong();
            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
            homeIntent.addCategory( Intent.CATEGORY_HOME );
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
        });
    }



}
