package cz.zcu.kiv.dicepoker.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import cz.zcu.kiv.dicepoker.FragmentBox;
import cz.zcu.kiv.dicepoker.R;
import cz.zcu.kiv.dicepoker.fragments.PlayBetAiFragment;
import cz.zcu.kiv.dicepoker.fragments.PlayEndRoundFragment;
import cz.zcu.kiv.dicepoker.fragments.PlayBetFragment;
import cz.zcu.kiv.dicepoker.fragments.PlayMainFragment;
import cz.zcu.kiv.dicepoker.game.Game;
import cz.zcu.kiv.dicepoker.game.GameSoundPlayer;
import cz.zcu.kiv.dicepoker.game.Player;
import cz.zcu.kiv.dicepoker.game.ai.EasyAIPlayer;
import cz.zcu.kiv.dicepoker.game.dice.Dice;

public class PlayActivity extends AppCompatActivity{
    /**
     * Instance úschovny pro odkazy na fragmenty
     */
    private FragmentBox fragmentBox;

    /**
     * Instance třída starající se o hru
     */
    private Game game = Game.getInstance();

    /**
     * Instance hráče
     */
    private Player player;

    /**
     * Umělá inteligence
     */
    private EasyAIPlayer ai;

    /**
     * Byla aktivita už jednou spuštěna?
     */
    private boolean activityStarted = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        this.fragmentBox = FragmentBox.getInstance();
        createFragments();
//
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        showBackDialog("Opravdu chceš ukončit hru a ztratit postup?", "Zpět do menu");
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(activityStarted == false){
            this.player = createPlayer();
            this.ai = createPlayerAi(player);
            game.setNewGame(ai, player, this);
            game.prepareStartRound();
            activityStarted = true;
            PlayMainFragment p = (PlayMainFragment)FragmentBox.getInstance().getFragment(PlayMainFragment.class.getName());
            p.setPlayerMoneyAmount(game.getPlayer().getMoney());
            p.setAiMoneyAmount(game.getAiPlayer().getMoney());
        }


        GameSoundPlayer.getInstance().playGameSong();
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus){
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }


    /**
     * Otevře jednoduché dialogové okno
     *
     * @param message zpráva dialogu
     * @param tittle nadpis dialogu
     */
    public void showBackDialog(String message, String tittle){
        AlertDialog.Builder builder = new AlertDialog.Builder(PlayActivity.this);
        builder.setPositiveButton("ANO", (dialog, id) -> {
            Intent intent = new Intent(PlayActivity.this, MainMenuActivity.class);
            startActivity(intent);
        });
        builder.setNegativeButton("NE", (dialog, id) -> {
        });
        builder.setMessage(message)
                .setTitle(tittle);
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    /**
     * Otevře fragment na základě identifikátoru který je metodě vložen
     * 
     * @param name pojmenování fragmentu
     */
    public void openFragment(String name){
        Fragment fragment = fragmentBox.getFragment(name);
        getSupportFragmentManager().beginTransaction().add(R.id.nav_host_fragment,fragment).commit();
    }

    /**
     * Zavře fragment
     * 
     * @param name pojmenování fragmentu
     */
    public void closeFragment(String name) {
        Fragment fragment = fragmentBox.getFragment(name);
        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
    }

    /**
     * Vytvoří instanci umělé inteligence 
     * 
     * @param player instance oponenta proti kterému bude ai hrát
     * @return umělá inteligence
     */
    private EasyAIPlayer createPlayerAi(Player player){
        Dice[] dices = new Dice[5];
        dices[0] = new Dice(findViewById(R.id.imageView_dice1_p2));
        dices[1] = new Dice(findViewById(R.id.imageView_dice2_p2));
        dices[2] = new Dice(findViewById(R.id.imageView_dice3_p2));
        dices[3] = new Dice(findViewById(R.id.imageView_dice4_p2));
        dices[4] = new Dice(findViewById(R.id.imageView_dice5_p2));

        return new EasyAIPlayer(dices, "Počítač", player, game);
    }

    /**
     * Vytvoří istanci hráče za kterýho hraje uživatel
     *
     * @return nový hráč
     */
    private Player createPlayer(){
        Dice[] dices = new Dice[5];
        dices[0] = new Dice(findViewById(R.id.imageView_dice1_p1));
        dices[1] = new Dice(findViewById(R.id.imageView_dice2_p1));
        dices[2] = new Dice(findViewById(R.id.imageView_dice3_p1));
        dices[3] = new Dice(findViewById(R.id.imageView_dice4_p1));
        dices[4] = new Dice(findViewById(R.id.imageView_dice5_p1));

        return new Player(dices, "Hráč", 500);
    }

    /**
     * Vytvoří fragmety a uloží je do instance fragment boxu přes, který lze
     * k nim přistupovat
     */
    private void createFragments(){
        PlayBetFragment playBetFragment = new PlayBetFragment();
        PlayBetAiFragment playBetAiFragment = new PlayBetAiFragment();
        PlayEndRoundFragment playEndRoundFragment = new PlayEndRoundFragment();

        fragmentBox.addFragment(PlayBetAiFragment.class.getName(), playBetAiFragment);
        fragmentBox.addFragment(PlayBetFragment.class.getName(), playBetFragment);
        fragmentBox.addFragment(PlayEndRoundFragment.class.getName(), playEndRoundFragment);
    }


    /**
     * Mělo by vyřešit občasné pády při daní aplikace do pazadí více
     * https://stackoverflow.com/questions/7469082/getting-exception-illegalstateexception-can-not-perform-this-action-after-onsa/10261438#10261438
     *
     * @param outState
     */
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }


    @Override
    protected void onStop() {
        super.onStop();
        GameSoundPlayer.getInstance().stopGameSong();
    }
}
