package cz.zcu.kiv.dicepoker.game.ai;

import java.util.Arrays;

import cz.zcu.kiv.dicepoker.FragmentBox;
import cz.zcu.kiv.dicepoker.fragments.PlayMainFragment;
import cz.zcu.kiv.dicepoker.game.Bet;
import cz.zcu.kiv.dicepoker.game.CombinationType;
import cz.zcu.kiv.dicepoker.game.Game;
import cz.zcu.kiv.dicepoker.game.GameSoundPlayer;
import cz.zcu.kiv.dicepoker.game.Player;
import cz.zcu.kiv.dicepoker.game.dice.Dice;

public class EasyAIPlayer extends Player {
    /**
     * Násobič rozdílu ve scóre mezi hráči
     */
    private static int combinationDifference = 4;

    /**
     * Násobič rozdílu ve scóre mezi hráči
     */
    private static int combinationBetDifference = 2;

    /**
     * Instance hráče
     */
    private Player opponent;

    /**
     * Instance hry
     */
    private Game game;

    /**
     * Vytvoří novou instanci umělé inteligence
     *
     * @param dices instance kostek umělé inteligence
     * @param name jméno ai
     * @param opponent instance oponenta
     * @param game instance hry
     */
    public EasyAIPlayer(Dice[] dices, String name, Player opponent, Game game) {
        super(dices, name, 500);
        this.opponent = opponent;
        this.game = game;
    }

    /**
     * Algoritmu umělé iinteligence pro vybrání kostek v
     * druhém kole hodu
     */
    public void selectedDices(){
        if(opponent.getScore() > getScore()){
            int opponentCombScore = (int)Math.floor(opponent.getScore());
            int aiCombScore = (int)Math.floor(getScore());
            int difference = opponentCombScore / aiCombScore;

            if(difference >= combinationDifference){
                Arrays.stream(getDices()).forEach(Dice::select);
            }else if(this.getCombinationType().equals(CombinationType.STRAIGHT)){
                Arrays.stream(getDices()).forEach(Dice::select);
            }else {
                Arrays.stream(getDices()).filter(d -> !d.isInCombination()).forEach(Dice::select);
            }
        }else{
            Arrays.stream(getDices()).filter(d -> !d.isInCombination()).forEach(Dice::select);
        }
    }

    /**
     * Umělá inteligence sázení
     *
     * @param isStartRound jde o počáteční kolo
     * @param roundBetPool vsazeno
     *
     * @return čas zvuku sázky
     */
    public int betAi(boolean isStartRound ,int roundBetPool){
        Bet bet = null;
        if(isStartRound == true){
            bet = Bet.getBetValue(roundBetPool);
        }else {
            bet = betCall(roundBetPool);
        }
        game.bet(bet, this);
        game.basicBet();
        GameSoundPlayer.getInstance().betSound(bet);
        return GameSoundPlayer.getInstance().getBetsSoundDuration(bet);
    }


    /**
     * V případě že jako první sází AI.
     * Podívám se jak si vede protihráč a pokud má lepší kombinaci, tak
     * si zjistím jak moc dobrou kombinaci má a podle toho vsadím. Pokud hodně dobrou,
     * tak nic, ne zas tak dobrou dám malou sázku jen o trochu lepší tak velkou sázku.
     *
     * @return sázka
     */
    private Bet basicBet(){
        Bet bet = null;
        if(opponent.getScore() > getScore()){
            int opponentCombScore = (int)Math.floor(opponent.getScore());
            int aiCombScore = (int)Math.floor(getScore());
            int difference = opponentCombScore / aiCombScore;

            if(difference > combinationDifference){
                bet = Bet.NO;
            }else if(difference > combinationBetDifference && difference < combinationDifference){
                bet = Bet.SMALL;
            }else {
                bet = Bet.BIG;
            }
        }else {
            bet = Bet.BIG;
        }

        return bet;
    }

    /**
     * Dorovnání nebo navíšení sázky protihráče.
     *
     * Pokud má hráč lepší scóre než jak to prostě jen dorovnám to co přihodil.
     *
     * Pokud ne tak podle toho jak moc mám já dobrou kombinaci vsadím,
     * velkou sázku nebo pokud pokud nemám až tak dobrou kombinaci tak jen
     * dám malou sázku, když teda protihráč něco nevsadil, pokud jo tak ho jen dorovnám
     *
     * @param pool počet peněz co už vsadil protihráč
     * @return sázka
     */
    private Bet betCall(int pool){
        Bet bet = null;
        if(opponent.getScore() > getScore()){
            bet = Bet.getBetValue(pool);
        }else{
            int opponentCombScore = (int)Math.floor(opponent.getScore());
            int aiCombScore = (int)Math.floor(getScore());
            int difference = opponentCombScore / aiCombScore;
            Bet opponentBet = Bet.getBetValue(pool);

            if(difference >= combinationDifference && opponentBet != Bet.BIG){
                bet = Bet.BIG;
            }else if (opponentBet != Bet.BIG && opponentBet != Bet.SMALL) {
                bet = Bet.SMALL;
            }else{
                bet = Bet.getBetValue(pool);
            }
        }
        return bet;
    }

    /**
     * Vsadí množství penět a vrátí vsazené peníze
     *
     * @param amount kolim se bude vsázet
     * @return vsazené množství peněz
     */
    @Override
    public int bet(int amount){
        PlayMainFragment p = (PlayMainFragment) FragmentBox.getInstance().getFragment(PlayMainFragment.class.getName());
        p.setAiMoneyAmount(money);
        if(money - amount <= 0){
            currentBet = money;
            money = 0;
            p.setAiMoneyAmount(money);
            return currentBet;
        }else {
            this.money -= amount;
            currentBet = amount;
            p.setAiMoneyAmount(money);
            return amount;
        }
    }

}
