package cz.zcu.kiv.dicepoker.game;

public enum Bet {
    NO, SMALL, BIG;

    /**
     * Na základě číselné hodnoty vrátí hodnotu
     * instanci příslušné BET
     *
     * @param bet sázka
     * @return vrátí odpovídající instanci bet
     */
    public static Bet getBetValue(int bet){
        switch (bet){
            case Game.SMALL_BLIND:
                return Bet.SMALL;
            case Game.BIG_BLIND:
                return Bet.BIG;
            default:
                return Bet.NO;
        }
    }
}
