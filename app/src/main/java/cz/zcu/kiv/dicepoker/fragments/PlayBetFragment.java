package cz.zcu.kiv.dicepoker.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import cz.zcu.kiv.dicepoker.FragmentBox;
import cz.zcu.kiv.dicepoker.activities.PlayActivity;
import cz.zcu.kiv.dicepoker.R;
import cz.zcu.kiv.dicepoker.game.Bet;
import cz.zcu.kiv.dicepoker.game.Game;
import cz.zcu.kiv.dicepoker.game.GamePhase;
import cz.zcu.kiv.dicepoker.game.GameSoundPlayer;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlayBetFragment extends Fragment {

    /**
     * Instance hry
     */
    private static Game game = Game.getInstance();

    /**
     * Tlačítko žádné sázky
     */
    private ImageButton noBet;

    /**
     * Tlačítko malé sázky
     */
    private ImageButton smallBet;

    /**
     * Tlačítko velké sázky
     */
    private ImageButton bigBet;

    /**
     * Tlačítko odmítnutí sázky
     */
    private ImageButton stop;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_play_bet, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createButtonsListeners(view);
        if(game.getGamePhase() == GamePhase.REISE){
            showCallOrRaiseButtons();
        }
    }

    /**
     * Zobrazí všechny tlačítka kromě odmítnutí sázky
     */
    public void showAllBetButton(){
        bigBet.setVisibility(View.VISIBLE);
        smallBet.setVisibility(View.VISIBLE);
        noBet.setVisibility(View.VISIBLE);
        stop.setVisibility(View.GONE);
    }

    /**
     * Zobrazí jen tlačítka pro dorovnání sázky nebo odmítnutí
     */
    public void showCallOrRaiseButtons(){
        int difference = Math.abs(game.getAiPlayer().getCurrentBet() - game.getPlayer().getCurrentBet());
        Bet bet = Bet.getBetValue(difference);

        bigBet.setVisibility(View.GONE);
        smallBet.setVisibility(View.GONE);
        noBet.setVisibility(View.GONE);
        stop.setVisibility(View.VISIBLE);

        switch (bet){
            case BIG:
                bigBet.setVisibility(View.VISIBLE);
                break;
            case SMALL:
                smallBet.setVisibility(View.VISIBLE);
                break;
            case NO:
                /*neráalný stav*/
        }

    }

    /**
     * Vytvoří listenery na tlačítka
     *
     * @param view view
     */
    private void createButtonsListeners(View view){
        noBlind(view);
        smallBlind(view);
        bigBlind(view);
        stopButton(view);
    }

    /**
     * Nastavení tlačítka odmítnutí sázky
     *
     * @param v view
     */
    @SuppressLint("ShowToast")
    private void stopButton(View v) {
        this.stop = v.findViewById(R.id.button_stop);
        stop.setOnClickListener(v1 -> {
            String mess = "Vážně chceš být za škrta a radši ukončit hru?";
            String tittle = "Odmítnutí přihození pěněz";
           FragmentActivity activity = getActivity();
           if(activity instanceof PlayActivity){
               ((PlayActivity)activity).showBackDialog(mess, tittle);
           }else {
               Toast.makeText(getContext(), R.string.error, Toast.LENGTH_LONG);
           }
        });
    }

    /**
     * Nastavení tlačítka "žádné sázky"
     *
     * @param v view
     */
    private void noBlind(View v){
        this.noBet = v.findViewById(R.id.button_no_blind);
        betButtonListeners(this.noBet, Bet.NO);
    }


    /**
     * Nastavení tlačíka malé sázky
     *
     * @param v view
     */
    private void smallBlind(View v){
        this.smallBet = v.findViewById(R.id.button_small_blind);
        betButtonListeners(smallBet, Bet.SMALL);
    }

    /**
     * Natavení tlačítka velké sázky
     *
     * @param v view
     */
    private void bigBlind(View v){
        this.bigBet = v.findViewById(R.id.button_big_blind);
        betButtonListeners(this.bigBet, Bet.BIG);
    }

    /**
     * Nastaví listennery na předané tlačítko pro sázení,
     *
     * Nastavuje onclick a on touch listener
     *
     * @param b  tlačítko
     * @param bet sázka
     */
    @SuppressLint("ClickableViewAccessibility")
    private void betButtonListeners(ImageButton b, Bet bet){
        b.setOnClickListener(v1 -> {
            enableButtons(false);
            game.bet(bet, game.getPlayer());
            int time = GameSoundPlayer.getInstance().getBetsSoundDuration(bet);
            GameSoundPlayer.getInstance().betSound(bet);
            Handler h = new Handler();
            h.postDelayed(() -> {
                game.refreshGame();
                enableButtons(true);
                showAllBetButton();
            }, time);
        });

        b.setOnTouchListener((v, event) -> {
            if(event.getAction() == MotionEvent.ACTION_DOWN){
                setBackgournd(bet, b, true);
            }
            if(event.getAction() == MotionEvent.ACTION_UP){
                setBackgournd(bet, b, false);
            }
            return false;
        });
    }

    /**
     * Nastaví tlačítko podle sázky a pozadí jestli má být jako že je zmáčklé nebo
     * v normální stavu
     *
     * @param bet výše sázky
     * @param button odkaz na obrázek tlačítka
     * @param pressed je zmáčklé?
     */
    private void setBackgournd(Bet bet, ImageButton button, boolean pressed){
        if(pressed){
            switch (bet){
                case BIG:
                    button.setImageResource(R.mipmap.big_bet_pressed_foreground);
                    break;
                case NO:
                    button.setImageResource(R.mipmap.no_bet_pressed_foreground);
                    break;
                case SMALL:
                    button.setImageResource(R.mipmap.small_bet_pressed_foreground);
                    break;
            }
        }else{
            switch (bet){
                case BIG:
                    button.setImageResource(R.mipmap.big_bet_foreground);
                    break;
                case NO:
                    button.setImageResource(R.mipmap.no_bet_foreground);
                    break;
                case SMALL:
                    button.setImageResource(R.mipmap.small_bet_foreground);
                    break;
            }
        }

    }

    /**,
     * Povolí nebo zakáze interakci s tlačítkama
     *
     * @param enable true povolí interakci false zakáže
     */
    private void enableButtons(boolean enable){
        this.bigBet.setEnabled(enable);
        this.smallBet.setEnabled(enable);
        this.noBet.setEnabled(enable);
    }
}
