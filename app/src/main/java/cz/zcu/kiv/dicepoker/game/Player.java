package cz.zcu.kiv.dicepoker.game;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.util.Log;
import android.view.View;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

import cz.zcu.kiv.dicepoker.FragmentBox;
import cz.zcu.kiv.dicepoker.fragments.PlayMainFragment;
import cz.zcu.kiv.dicepoker.game.dice.Dice;
import cz.zcu.kiv.dicepoker.game.dice.DiceValue;
import cz.zcu.kiv.dicepoker.game.dice.DiceValues;

public class Player {
    /* Instance generátoru náhodných čísel */
    private static Random rnd = new Random();

    /* Instance kostek */
    private Dice[] dices;

    /* Aktuální hozená kombinace hráčem */
    private CombinationType combinationType;

    /* Jméno hráče */
    private String name;

    /* aktuální scére hráče */
    private int score;

    /* Počet vyhraných kol */
    private int roundWins;

    /* počet peněz hráče */
    protected int money;

    /* Sázka v aktuálním kole */
    protected int currentBet;


    /**
     * Vytvoří novou instanci hráče s jeho kostkami jménem a
     * pěnězi kolik jich má hráč k dispozici
     *
     * @param dices kostky hráče
     * @param name jméno hráče
     * @param money počet peněz hráče
     */
    public Player(Dice[] dices, String name, int money) {
        this.dices = dices;
        this.score = 0;
        this.money = money;
        this.roundWins = 0;
        this.name = name;
    }

    /**
     * Hodí všemi označenýma kostkama a získá tak nové
     * randomizované hodnoty
     */
    public void rollDices(){
        for(Dice dice : dices){
            if(dice.isSelected() == true){
                int randomNum = rnd.nextInt(6) + 1;
                DiceValue randomVal = DiceValues.getInstance().getValue(randomNum);
                dice.setValue(randomVal);
            }
        }
    }

    /**
     * Vsadí množství penět a vrátí vsazené peníze
     *
     * @param amount kolim se bude vsázet
     * @return vsazené množství peněz
     */
    public int bet(int amount){
        PlayMainFragment p = (PlayMainFragment) FragmentBox.getInstance().getFragment(PlayMainFragment.class.getName());
        if(money - amount <= 0){
            currentBet = money;
            money = 0;
            p.setPlayerMoneyAmount(money);
            return currentBet;
        }else {
            this.money -= amount;
            currentBet = amount;
            p.setPlayerMoneyAmount(money);
            return amount;
        }
    }

    /**
     * Vrátí množství pěněz co má hráč
     *
     * @return
     */
    public int getMoney(){
        return money;
    }

    /**
     * Vrátí počet označených kostek
     *
     * @return vrátí počet oznaačených kostek
     */
    public int getNumberOfSelectedDices(){
        return (int)Arrays.stream(dices).filter(Dice::isSelected).count();
    }

    /**
     * Vrátí kostky hráče
     *
     * @return kostky hráče
     */
    public Dice[] getDices() {
        return dices;
    }

    /**
     * Vrátí jméno hráče
     *
     * @return jméno hráče
     */
    public String getName(){
        return name;
    }

    /**
     * Vrátí skóre kombinace hráče
     *
     * @return skóre kombinace
     */
    public int getScore() {
        return score;
    }

    /**
     * Podle hozené kombinace nastaví hráči aktuální skóre kombinace
     *
     * @param combi hozená kombinace skore a typ
     */
    public void setScore(Combination combi) {
        this.combinationType = combi.type;
        this.score = combi.combinationScore + calculateDicesSum();
    }

    /**
     * Vyresetuje skóre kombinace
     */
    public void resetScore(){
        this.score = 0;
    }

    /**
     * Vyresetuje hodnoty kostek na 1
     */
    public void resetDicesValue(){
        Arrays.stream(dices).forEach(d -> d.setValue(DiceValues.getInstance().getValue(1)));
    }

    /**
     * Nastaví kostky zpátky na to, že v kombinaci nejsou
     */
    public void resetInCombinationDices(){
        Arrays.stream(dices).forEach(d -> d.setInCombination(false));
    }

    /**
     * Označí všechny kostky
     */
    public void selectAllDices(){
        Arrays.stream(dices).forEach(Dice::select);
    }

    /**
     * Odznačí všechny kostky
     */
    public void unselectAllDices(){
        Arrays.stream(dices).forEach(Dice::unselect);
    }

    /**
     * Ozančí nejlepší hozenou kombinaci a
     * na kostky které jsou v kombinaci předá zelený filtr
     */
    public void highlightBestCombination(){
        for(Dice dice : dices){
            if(dice.isInCombination()){
                dice.diceResource.setColorFilter(Color.argb(60, 0, 255, 0));
            }
        }
    }

    /**
     * Odznačí kostky které jsou v kombinaci a zruší jejich barevný filtr  (zelený)
     */
    public void unHighlightDices(){
        Arrays.stream(dices).forEach(d -> {
            d.diceResource.setColorFilter(Color.argb(0,0,0,0));
        });
    }

    @SuppressLint("ResourceType")
    public void makeSelectedDicesInvisible(boolean invisible) {
        if(invisible){
            Arrays.stream(dices).filter(Dice::isSelected).forEach(dice -> {
                    dice.diceResource.setVisibility(View.INVISIBLE);
                    Log.d("sakra", dice.getDiceResource().getId() + "");
            });
        }else {
            for(Dice d : dices){
                Log.d("sakra", d.getDiceResource().getId() + "");
                d.diceResource.clearAnimation();
                d.diceResource.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * Přidá vítězství v koole
     */
    public void victoryInRound(){
        this.roundWins++;
    }

    /**
     * Vrátí počet vítězství
     *
     * @return počet vízezství
     */
    public int getRoundWins(){
        return this.roundWins;
    }

    /**
     * Vyresetuje počet výtězství
     */
    public void resetRoundWins(){
        this.roundWins = 0;
    }

    /**
     * Nastaví aktuální vsazenou hodnotu na 0
     */
    public void setCurrentBetToZero(){
        this.currentBet = 0;
    }

    /**
     * Vrátí aktuálně vsazenou částku
     *
     * @return vsazená částka
     */
    public int getCurrentBet(){
        return this.currentBet;
    }

    /**
     * Vrátí o jakou jde kombinaci
     *
     * @return typ kombinace
     */
    public CombinationType getCombinationType(){
        return combinationType;
    }

    /**
     * Vratá kostku s nejvyšší hodnotou
     *
     * @return kostka s nejvyšší hodnotou
     */
    private int getHighestDice(){
        int highest = Integer.MIN_VALUE;
        int current;

        for (Dice dice : dices) {
            current = dice.getValue().value;
            if (current > highest) {
                highest = current;
            }
        }

        return highest;
    }

    /**
     * Vypočítá sumu hodnot kostek které nejsou v kombinaci
     *
     * @return součet hozených čísel
     */
    private int calculateDicesSum(){
        return (int)Arrays.stream(dices).filter(dice -> !dice.isInCombination()).count();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return score == player.score &&
                money == player.money &&
                Arrays.equals(dices, player.dices) &&
                Objects.equals(name, player.name);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, score, money);
        result = 31 * result + Arrays.hashCode(dices);
        return result;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", score=" + score +
                ", roundWins=" + roundWins +
                ", money=" + money +
                ", currentBet=" + currentBet +
                '}';
    }
}
